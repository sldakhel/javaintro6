package escape_sequences;

public class Exercise03 {
    public static void main(String[] args) {
        System.out.println("--------TASK-1-------");
        System.out.println("Monday\\Tuesday\\Wednesday");
        System.out.println("-------TASK-2------");
        System.out.println("Good\\\\\\ Morning");
        System.out.println("");
    }
}
