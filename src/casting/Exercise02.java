package casting;

public class Exercise02 {
    public static void main(String[] args) {
        // you want to buy a phone for $900, if you save $50 every day,
        // in how many days can you purchase the phone?

        //you can buy the phone in 18 days


        //store the price in a double

        double price = 900;
        double dailySaveAmount = 50;



        System.out.println("You can buy the phone after " + (int) (price/ dailySaveAmount) + " days.");
    }
}
