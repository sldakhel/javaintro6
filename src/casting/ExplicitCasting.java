package casting;

public class ExplicitCasting {
    public static void main(String[] args) {

        /*
        Explicit casting is storing bigger data types into smaller data types.
        It doesn't happen automatically, the programmer has to resolve the compiler issue.
        Also known as narrowing or down-casting.
        long--> byte
        int --> short
        double --> float
        double --> int
         */

        long number1 = 273645273;
        byte number2 = (byte) number1;
    }
}
