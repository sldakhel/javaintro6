package mathClass;

public class FindSquareRoot {
    public static void main(String[] args) {
        // 5^2 == 25
        // Square root of 25 is 5

        System.out.println(Math.sqrt(25));


        System.out.println(Math.round(5.5));//6
        System.out.println(Math.round(45.3));//45
        System.out.println(Math.round(1.49999999));//1
    }
}
