package mathClass;

public class FindMax {
    public static void main(String[] args) {
        // finding the max of to numbers
        int num1 = 10;
        int num2 = 15;


        int max = Math.max(num1, num2);
        System.out.println(max);


        // finding the max of four numbers

        int number1 = 2;
        int number2 = 8;
        int number3 = 5;
        int number4 = 18;

        // find the max between number1 and number2 which is 8. then find the max between number3 and number 4
        // which is 18. then find the max between those two numbers.

        int max1 = Math.max(number1, number2);
        int max2 = Math.max(number3, number4);
        int finalMax = Math.max(max1 , max2);

        System.out.println(Math.max(max1 , max2));

        //System.out.println(Math.max(Math.max(number1, number2), Math.max(number3, number4)));


        //finding the max of 3 numbers
        number1 = -30;
        number2 = -40;
        number3 = 0;

        max1 = Math.max(number1, number2);

        System.out.println(Math.max(max1, number3));


        //find the max of 5 numbers

        int a = 5;
        int b = 10;
        int c = 50;
        int d = 189;
        int e = 12;

        //(Math.max(Math.max(a,b), Math.max(c,d))); = the max of the first 4 numbers








    }
}
