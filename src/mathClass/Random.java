package mathClass;

public class Random {
    public static void main(String[] args) {
        System.out.println(Math.random());// just gives you a random number between 0.0 - 1.0 (always less than 1)

        System.out.println(Math.random() * 10); // generates random number from 0.0 - 10.0 ( does not include 10)
        System.out.println(Math.random() * 11); // generates random number from 0.0 - 11.0 ( does not include 11)
        System.out.println(Math.random() * 20); // generates random number from 0.0 - 20.0 ( does not include 20)

        System.out.println(Math.round(Math.random() * 10)); //this will include 10 because it will round it up to 10

        System.out.println(Math.round(Math.random() * 25));

        // Find how many numbers in your range: biggest - smallest + 1

        // to get numbers between a given range
        // get a random number between 17 and 53  (both inclusive)
        // 53 - 17 + 1 --> 37 numbers between 17 and 53

        // Then multiply your random result with the number
        //(int) (Math.random)() * 37) --> 0 and 36 ( both inclusive)

        // Add smallest number to yur result
        // (int)( Math.random)()*37 + 17 --> 17 ad 53 (both inclusive)

        







       
    }


}
