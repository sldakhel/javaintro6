package strings;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class UnderstandingStrings {
    public static void main(String[] args) {
        String s1; // declaration of s1 as a String
        s1 = "techGlobal School"; // initializing s1 as tehGlobal School

        String s2 = "is the best"; //initializing s2 as is the best

        System.out.println(s1);
        System.out.println(s2);
        System.out.println("---------CONCAT USING +----------\n");

        String s3 = s1 + " " + s2;
        System.out.println(s3);

        System.out.println("------------CONCAT USING METHOD--------\n");
        String s4 = s1.concat(s2);


    }









}
