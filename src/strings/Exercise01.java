package strings;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        String is a reference type (object) that is used to store a sequence of characters TEXTS
         */

        String name = "John";
        String address = "Chicago IL 12345";
        System.out.println(name);
        System.out.println(address);
        String favMovie = "Jurassic Park";
        System.out.println("My favorite movie = " + favMovie);




    }
}
