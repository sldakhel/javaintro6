package exceptions;

import utilities.ScannerHelper;

public class _01_Unchecked_Runtime_Exceptions {
    public static void main(String[] args) {

        String name = "John";

        int [] numbers = {10, 15, 20};

        System.out.println(numbers[5]);//ArrayIndexOutOfBoundsException

        System.out.println(name.charAt(5));// StringIndexOutOfBoundsException

        String address = null;
        System.out.println(address.toUpperCase());// NullPointerException

        System.out.println("End of the program");


        String name1 = ScannerHelper.getFirstName();

        try{
            System.out.println(name1.charAt(5)); // StringIndexOutOfBoundsException
        }catch (StringIndexOutOfBoundsException e){
            System.out.println("I could not print char at 5 as it does not exist!");
        }


        System.out.println("The rest of the program");

        try{ // using try-catch to handle the exception
            System.out.println(name.charAt(5)); // StringIndexOutOfBoundsException
        }catch (StringIndexOutOfBoundsException e){
            System.out.println("I could not print char at 5 as it does not exist!");
        }


        System.out.println("The rest of the program");
    }
}
