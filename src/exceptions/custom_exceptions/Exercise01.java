package exceptions.custom_exceptions;

public class Exercise01 {
    public static void main(String[] args) {
        int day = 5;
        try {
            if (isCheckInHours(day)) {
                System.out.println("The input is a valid day!");
            }
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

    }

    public static boolean isCheckInHours(int day) {
        if (day >= 1 && day <= 7) {
            return true;
        } else {
            throw new RuntimeException("The input does not represent any day!!!");
        }
    }
}
