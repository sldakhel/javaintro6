package operators.arithmetic_operators;

public class Excersice02 {
    public static void main(String[] args) {

        double averageSalary = 90000;
        System.out.println(" Monthly = $" + averageSalary / 12);
        System.out.println(" Bi-weekly = $" + averageSalary / 26);
        System.out.println(" Weekly = $" + averageSalary/ 52);


    }
}
