package operators.arithmetic_operators;

public class MathFunctions {
    public static void main(String[] args) {
        int num1 = 9, num2 = 3;

        int sum = num1 + num2;
        System.out.println(sum);

        int multiplication = num1 * num2;
        System.out.println(multiplication);

        int subtraction = num1 - num2;
        System.out.println(subtraction);

        int division = num1 / num2;
        System.out.println(division);

        int remainder = num1 % num2;
        System.out.println(remainder);





    }
}
