package operators.shorthand_operators;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        //Pseudo Code
        // Create a Scanner object
        //First, ask user to enter their balance
        //Second, ask them to enter the transactions
        //Finally, print the balance after each transaction


        Scanner inputReader = new Scanner(System.in);

        System.out.println(" Hello, please enter your balance: ");
        double balance = inputReader.nextDouble();

        System.out.println("Initial balance = $" + balance);

        System.out.println("What is the first transaction amount?");
        double firstTrns = inputReader.nextDouble();

        balance -= firstTrns;

        System.out.println(" The balance after first transaction = $" + balance);

        System.out.println("What is the second transaction amount?");
        double secondTrns = inputReader.nextDouble();

        balance -= secondTrns;

        System.out.println(" The balance after second transaction = $" + balance);

        System.out.println("What is the third transaction amount?");
        double thirdTrns = inputReader.nextDouble();

        balance -= thirdTrns;

        System.out.println(" The balance after third transaction = $" + balance);



    }
}
