package variables;

public class UnderstandingVariables {
    public static void main(String[] args) {
        int age = 25;
        System.out.println(age);
        String name = "John"; // declaring the variable without a value
        System.out.println(name);
        age = 50; // initializing the variable

        // capitalize letters and lowercase letters are seen differently with java

        double money = 5.5;
        double mOney = 4.5;
        double Money;

        double d1 = 10;
        System.out.println(d1);




    }
}
