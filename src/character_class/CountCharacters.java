package character_class;

import utilities.ScannerHelper;

public class CountCharacters {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();
        int digits = 0;
        int letters = 0;

        for (int i = 0; i <= str.length() - 1; i++) {
            if (Character.isDigit(str.charAt(i))) digits++;
            else if (Character.isLetter(str.charAt(i))) letters++;

        }
        System.out.println("The string has " + digits + " digits and " + letters + " letters");

        int upperCase = 0;
        int lowerCase = 0;

        for (int i = 0; i <= str.length() - 1; i++) {
            if (Character.isUpperCase(str.charAt(i))) upperCase++;
            else if (Character.isLowerCase(str.charAt(i))) lowerCase++;
        }
        System.out.println("The string has " + upperCase + " upperCase letters and " + lowerCase + " lowerCase letters.");


        int special = 0;
        for (int i = 0; i <= str.length() - 1; i++) {
            char c = str.charAt(i);// if c is not a digit, not a letter, not a space
            if (!Character.isLetterOrDigit(c) && !Character.isWhitespace(c)) ;
            System.out.println(special);
        }


        int letter = 0;
        int uppercaseLetter = 0;
        int lowercaseLetter = 0;
        int digit = 0;
        int spaces = 0;
        int specialChars = 0;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if (Character.isLetter(c)) {
                letter++;
                if (Character.isUpperCase(c)) {
                    uppercaseLetter++;
                } else {
                    lowercaseLetter++;
                }
            } else if (Character.isDigit(c)) {
                digit++;
            } else if (Character.isWhitespace(c)) {
                spaces++;
            } else {
                specialChars++;


            }

        }


    }
}
