package character_class;

import utilities.ScannerHelper;

import java.util.Locale;

public class UnderstandingCharacterClass {
    public static void main(String[] args) {
        /*
        char is primitive data type
        Character is a wrapper class
        Character is object representation char primitive


        Wrapper classes provide us with methods that allow us to manipulate the data
         */

        String str = ScannerHelper.getString();
        //Print true if str starts with upper case, print false if otherwise

        char firstChar = str.charAt(0);
        System.out.println(65 <= firstChar && firstChar <= 90);
        //Instead of decimal values from ASCII
        //Use Character class and useful methods
        System.out.println(Character.isLowerCase(firstChar));




    }
}
