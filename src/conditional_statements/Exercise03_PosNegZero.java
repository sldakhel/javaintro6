package conditional_statements;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Exercise03_PosNegZero {
    public static void main(String[] args) {
        Scanner number = new Scanner(System.in);

        System.out.println("Please enter a number.");
        int digit = number.nextInt();

        if(digit > 0){
            System.out.println("POSITIVE");
        }
        else if(digit < 0){
            System.out.println("NEGATIVE");
        }
        else{
            System.out.println("ZERO");
        }
    }
}
