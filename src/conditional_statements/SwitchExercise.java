package conditional_statements;

import java.util.Scanner;

public class SwitchExercise {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a day of the week in lowercase format");
        String day = input.nextLine();

        switch (day) {
            case "monday": {
                System.out.println("First day of the week");
                break;
            }
            case "tuesday": {
                System.out.println("Second day of the week");
                break;
            }
            case "wednesday": {
                System.out.println("Third day of the week");
                break;
            }
            case "thursday": {
                System.out.println("Fourth day of the week");
                break;
            }
            case "friday": {
                System.out.println("Fifth day of the week");
                break;
            }
            case "saturday": {
                System.out.println("sixth day of the week");
                break;
            }
            case "sunday": {
                System.out.println("seventh day of the week");
                break;
            }
            default: {
                System.out.println("ERROR");
            }
        }
    }
 }
