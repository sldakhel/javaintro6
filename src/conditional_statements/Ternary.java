package conditional_statements;

import java.util.Scanner;

public class Ternary {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);


        System.out.println("Please enter a number");
        int number = input.nextInt();

        System.out.println((number % 2 != 1) ? ("even") : ("odd"));



    }
}
