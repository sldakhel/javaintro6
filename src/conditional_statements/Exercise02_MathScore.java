package conditional_statements;

import java.util.Scanner;

public class Exercise02_MathScore {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your math score.");
       int number = input.nextInt();

       if(number >= 60){
           System.out.println("Awesome! You have passed the math class!");
       }
       else{
           System.out.println("Sorry! You failed!");
       }
    }
}
