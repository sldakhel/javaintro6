package conditional_statements;

import java.util.Scanner;

public class Exercise04_RetirementAge {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Pleas enter your age.");
        int age = input.nextInt();
        System.out.println("Is it your time to get retired!");


        if(age >= 55){
            System.out.println("It's time to retire!");

        }

        else{

            System.out.println("you have  " + (55 - age) + " years to be retired.");
        }
    }
}
