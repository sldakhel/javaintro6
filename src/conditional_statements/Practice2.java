package conditional_statements;

public class Practice2 {
    public static void main(String[] args) {
        int randomNumber = (int)( Math.random() * 51);
        System.out.println(randomNumber);

        if(randomNumber >= 10 && randomNumber <=25) System.out.println(true);
        else System.out.println(false);

        //OR

        System.out.println(randomNumber >= 10 && randomNumber <=25);



    }
}
