package conditional_statements;

import java.util.Scanner;

public class Exercise05_CheckAllEven {
    public static void main(String[] args) {
        System.out.println("Please enter 3 numbers.");
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        input.nextInt();
        input.nextInt();

        if (number % 2 == 0){
            System.out.println("EVEN");
        }

        else{
            System.out.println("ODD");

        }
    }
}
