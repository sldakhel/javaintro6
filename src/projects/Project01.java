package projects;

import java.util.Scanner;

public class Project01 {
    public static void main(String[] args) {
        System.out.println("\n--------Task1--------\n");

        String name = "Sandina";
        System.out.println(" My name is " + name );

        System.out.println("\n--------TASK2--------\n");
        char nameCharacter1 = 'S';
        char nameCharacter2 = 'a';
        char nameCharacter3 = 'n';
        char nameCharacter4 = 'd';
        char nameCharacter5 = 'i';
        char nameCharacter6 = 'n';
        char nameCharacter7 = 'a';

        System.out.println("Name letter 1 is " + nameCharacter1);

        System.out.println("Name letter 2 is " + nameCharacter2);

        System.out.println("Name letter 3 is " + nameCharacter3);

        System.out.println("Name letter 4 is " + nameCharacter4);

        System.out.println("Name letter 5 is " + nameCharacter5);

        System.out.println("Name letter 6 is " + nameCharacter6);

        System.out.println("Name letter 7 is " + nameCharacter7);


        System.out.println("\n--------TASK3--------\n");

        String favMovie = "Jurassic Park.";
        String favSong = "Home.";
        String favCity = "Laguna Beach.";
        String favActivity = "Hiking.";
        String favSnack = "ice cream.";

        System.out.println("My favorite movie is " + favMovie );

        System.out.println("My favorite song is " + favSong);

        System.out.println("My favorite city is " + favCity);

        System.out.println("My favorite activity is " + favActivity);

        System.out.println("My favorite snack is " + favSnack);


        System.out.println("\n--------TASK4--------\n");


        int num1 = 4;
        int num2 = 7;
        int num3 = 6;

        System.out.println("My favorite number is " + num1);

        System.out.println("I have visited " + num2 + " " +"states.");

        System.out.println("I have visited " + num3 + " " +"countries.");


        System.out.println("\n--------TASK5--------\n");

        boolean amIAtSchoolToday = false;
        System.out.println("I am at school today." + amIAtSchoolToday);



        System.out.println("\n------TASK6------\n");

        boolean isWeatherNiceToday = false;
        System.out.println("Weather is nice today." + isWeatherNiceToday);











    }
}
