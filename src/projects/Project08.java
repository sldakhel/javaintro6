package projects;

import java.util.Arrays;

public class Project08 {

    // Task 1
    public static int findClosestDistance(int[] arr) {
        if (arr == null || arr.length < 2) {
            return -1; // array does not have at least 2 elements
        }
        Arrays.sort(arr);
        int closest = arr[1] - arr[0];
        for (int i = 1; i < arr.length - 1; i++) {
            int diff = arr[i + 1] - arr[i];
            if (diff < closest) {
                closest = diff;
            }
        }
        return closest;
    }

        // Task 2
    public static int findSingleNumber(int[] arr) {
        if (arr == null || arr.length == 0) {
            throw new IllegalArgumentException("Array must be non-empty");
        }
        int result = 0;
        for (int num : arr) {
            result = num;
        }
        return result;
    }


    // Task 4
    public static int findMissingNumber(int[] arr) {
        if (arr == null || arr.length < 2) {
            System.out.println("Array must have at least 2 elements");
            return -1;
        }
        int expectedSum = 0;
        int actualSum = 0;
        for (int i = 0; i < arr.length + 1; i++) {
            expectedSum += i + 1;
        }
        for (int j : arr) {
            actualSum += j;
        }
        return expectedSum - actualSum;
    }




}
