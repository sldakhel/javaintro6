package projects;



import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n------TASK1------\n");

        System.out.println("Please enter 3 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        int num3 = input.nextInt();

        System.out.println("The product of the numbers entered is =" + num1 * num2 * num3);


        System.out.println("\n------TASK2------\n");

        System.out.println("Please enter your first name.");
        String firstName = input.nextLine();


        System.out.println("Please enter your last name.");
        String lastName = input.nextLine();

        System.out.println("Please enter your birth year.");
        int birthYear = input.nextInt();


        System.out.println(firstName +" " + lastName +"'s age is " + (2023  +- birthYear));


        System.out.println("\n------TASK3------\n");

        System.out.println("Please enter your full name.");
        String fullName = input.nextLine();

        System.out.println("Please enter your weight.");
        double kg = input.nextDouble();

        System.out.println(fullName + "'s weight is " + (70 * 2.205) + "lbs."  );


        System.out.println("\n------TASK4------\n");

        System.out.println("Student 1, please enter your full name.");
        String fName1 = input.nextLine();
        System.out.println("What is you age?");
        int age1 = input.nextInt();
        System.out.println(fName1 + "'s age is " + age1);

        System.out.println("Student 2 please enter your full name.");
        input.nextLine();
        String fName2 = input.nextLine();
        System.out.println("What is you age?");
        int age2 = input.nextInt();
        System.out.println(fName2 + "'s age is " + age2);

        System.out.println("Student 3 please enter your full name.");
        input.nextLine();
        String fName3 = input.nextLine();
        System.out.println("What is you age?");
        int age3 = input.nextInt();
        System.out.println(fName3 + "'s age is " + age3);


        int avgAge = (age1 + age2 + age3) / 3;
        System.out.println("The average age is " + avgAge);

        int max1 = Math.max(age1,age2);
        int finalMax = Math.max(max1,age3);

        System.out.println("The eldest age is " + finalMax);

        int min1 = Math.min(age1,age2);
        int finalMin = Math.min(min1,age3);
        System.out.println("The youngest age is " + finalMin);














    }
}
