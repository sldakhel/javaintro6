package projects;

import utilities.ScannerHelper;

import java.sql.SQLOutput;


public class Project04 {
    public static void main(String[] args) {

        System.out.println("\n------Task1------\n");

        String str = ScannerHelper.getString();
        //str will be the string that the user enters
        if (str.length() < 8) System.out.println("The string does not have 8 characters");
        else System.out.println(str.substring(str.length() - 4) + str.substring(0, 5));

        System.out.println("\n------Task2------\n");

        String sentence = ScannerHelper.getString();
        String words = sentence.trim();
        if (words.lastIndexOf(" ") <= -1) System.out.println("This sentence does not have 2 or more words to swap");
        else {
            String firstWord = words.substring(0, words.indexOf(" "));
            String lastWord = words.substring(words.lastIndexOf(" ") + 1, words.length());
            if (words.indexOf(" ") == words.lastIndexOf(" ")) System.out.println(lastWord + " " + firstWord);
            else {
                String middle = words.substring(words.indexOf(" ") + 1, words.lastIndexOf(" "));
                System.out.println(lastWord + " " + middle + " " + firstWord);
            }
        }

        System.out.println("\n------Task3-------\n");

        String str1 = ScannerHelper.getString();
        System.out.println(str1.replace("stupid", "nice").replace("idiot", "nice"));

        System.out.println("\n------Task4-------\n");

        String name = ScannerHelper.getString();
        if (name.length() < 2) System.out.println("Invalid input!!");
        else if (name.length() % 2 == 0)
            System.out.println(name.substring(name.length() / 2 - 1, name.length() / 2 + 1));
        else System.out.println(name.substring((name.length() / 2), name.length() / 2 + 1));

        System.out.println("\n------Task5-------\n");

        String country = ScannerHelper.getString();
        if (country.length() <= 5) System.out.println("Invalid input!!!");
        else System.out.println(country.substring(2, country.length() - 2));

        System.out.println("\n------Task6-------\n");

        String address = ScannerHelper.getString();
        String address1 = address.toLowerCase();
        System.out.println(address1.replace("a", "*").replace("e", "#").replace("i", "+").replace("u", "$").replace("o", "@"));


    }
}