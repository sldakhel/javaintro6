package projects;


import sun.font.TrueTypeFont;
import utilities.ScannerHelper;

import java.util.Random;

public class Project05 {
    public static void main(String[] args) {

        System.out.println("\n------Task1------\n");

          String str = ScannerHelper.getString();
        str =str.trim();
        if (!str.contains(" ")) System.out.println("This sentence does not have multiple words");
        else {
        int words = 1;
        for (int i = 0; i < str.length() ; i++) {
            if (str.charAt(i) == ' ') words++;
        }
            System.out.println("This sentence has " + words + " words.");
        }

        System.out.println("\n------Task2------\n");
        Random r = new Random();
        int num1 = r.nextInt(26);

        System.out.println("\n------Task3------\n");

        String sentence = ScannerHelper.getString();
        int count = 0;
        sentence = sentence.toLowerCase();
        if(sentence.length() == 0) System.out.println("This sentence does not have any characters");
        else
        for (int i = 0; i < sentence.length()-1; i++) {
            if (sentence.charAt(i)== 'a') count++;
        }
        System.out.println("This sentence has " +count + " a or A letters.");


        System.out.println("\n------Task4------\n");

        String str2 = ScannerHelper.getString();
        boolean check = true;

        if(str2.length() == 0) System.out.println("The word does not have 1 or more characters.");
        else{
            for (int i = 0; i <= str2.length()-1-i; i++) {
                if (str2.charAt(i) != str2.charAt(str2.length() - 1 - i)) {
                    check = false;
                    break;
                }
            }
            if(check) System.out.println("This word is palindrome");
            else System.out.println("This word is not palindrome");
        }

            System.out.println("\n------Task5------\n");

            for (int j = 0; j < 9; j++) {
                for (int i = 0; i < 8-j; i++)
                    System.out.print(" ");
                for (int k = 0; k < (2*j)+1; k++)
                    System.out.print("*");
                System.out.println();
            }





    }


        
            
        


    }

