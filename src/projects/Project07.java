package projects;

import java.util.ArrayList;

public class Project07 {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(2);
        numbers.add(-5);
        numbers.add(6);
        numbers.add(7);
        numbers.add(-10);
        numbers.add(-78);
        numbers.add(0);
        numbers.add(15);

        ArrayList<Integer> result = removeNegatives(numbers);

        System.out.println(result);
    }



    public static int countMultipleWords(String[] arr) {
        int count = 0;
        for (String str : arr) {
            if (str.trim().split(" ").length > 1) {
                count++;
            }
        }
        return count;
    }

    public static ArrayList<Integer> removeNegatives(ArrayList<Integer> numbers) {
        ArrayList<Integer> result = new ArrayList<>();
        for (Integer number : numbers) {
            if (number >= 0) {
                result.add(number);
            }
        }
        return result;
    }

    public static boolean validatePassword(String password) {

        if (password.length() < 8 || password.length() > 16) {
            return false;
        }


        boolean hasDigit = false;
        boolean hasUpper = false;
        boolean hasLower = false;
        boolean hasSpecial = false;

        for (char ch : password.toCharArray()) {
            if (Character.isDigit(ch)) {
                hasDigit = true;
            } else if (Character.isUpperCase(ch)) {
                hasUpper = true;
            } else if (Character.isLowerCase(ch)) {
                hasLower = true;
            } else if ("!@#$%^&*()_-+=[{]}\\|;:'\",<.>/?".indexOf(ch) != -1) {
                hasSpecial = true;
            }
        }

        if (!hasDigit || !hasUpper || !hasLower || !hasSpecial) {
            return false;
        }


        return password.indexOf(' ') == -1;


    }



}
