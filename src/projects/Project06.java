package projects;

import java.util.Arrays;

public class Project06 {
    public static void main(String[] args) {

        System.out.println("\n------Task1------\n");
        int[] numbers = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithSort(numbers);


        System.out.println("\n------Task2------\n");
        int[] numbers2 = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallest(numbers2);


        System.out.println("\n------Task3------\n");
        int[] numbers3 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSecondSmallest(numbers3);


        System.out.println("\n------Task4------\n");
        int[] arr = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestSecondSmallest(arr);
    }


    public static void findGreatestAndSmallestWithSort(int[] numbers) {
        System.out.println("\n------Task1------\n");
        Arrays.sort(numbers);

        System.out.println("Smallest = " + numbers[0]);
        System.out.println("Greatest = " + numbers[numbers.length - 1]);

    }
    public static void findGreatestAndSmallest(int[] numbers2) {
        System.out.println("\n------Task2------\n");
        int smallest = numbers2[0];
        int greatest = numbers2[0];

        for (int i = 1; i < numbers2.length; i++) {
            if (numbers2[i] < smallest) {
                smallest = numbers2[i];
            }
            if (numbers2[i] > greatest) {
                greatest = numbers2[i];
            }
        }

        System.out.println("Smallest = " + smallest);
        System.out.println("Greatest = " + greatest);
    }

    public static void findSecondGreatestAndSecondSmallest(int[] numbers3) {
        System.out.println("\n------Task3------\n");
        Arrays.sort(numbers3);
        int n = numbers3.length;

        int secondSmallest = numbers3[1];
        int secondGreatest = numbers3[n - 2];

        System.out.println("Second Smallest = " + secondSmallest);
        System.out.println("Second Greatest = " + secondGreatest);
    }

    public static void findSecondGreatestSecondSmallest(int[] numbers4) {
        System.out.println("\n------Task4------\n");
        int smallest = Integer.MAX_VALUE;
        int secondSmallest = Integer.MAX_VALUE;
        int greatest = Integer.MIN_VALUE;
        int secondGreatest = Integer.MIN_VALUE;

        for (int i : numbers4) {
            if (i < smallest) {
                secondSmallest = smallest;
                smallest = i;
            } else if (i < secondSmallest) {
                secondSmallest = i;
            }

            if (i > greatest) {
                secondGreatest = greatest;
                greatest = i;
            } else if (i > secondGreatest) {
                secondGreatest = i;
            }
        }

        System.out.println("Second Smallest = " + secondSmallest);
        System.out.println("Second Greatest = " + secondGreatest);
    }

    public static void findDuplicates(String[] words) {
        System.out.println("\n------Task5------\n");
        Arrays.sort(words);

        for (int i = 0; i < words.length - 1; i++) {
            if (words[i].equals(words[i+1])) {
                System.out.println(words[i]);
            }
        }
    }




}
