package projects;

import java.sql.SQLOutput;
import java.util.Random;

public class Project03 {
    public static void main(String[] args) {

        System.out.println("\n------TASK1------\n");

        String s1 = "24", s2 = "5";

        System.out.println("The sum of 24 and 5 = " + (Integer.parseInt(s1) + Integer.parseInt(s2)));
        System.out.println("The subtraction of 24 and 5 = " + (Integer.parseInt(s1) - Integer.parseInt(s2)));
        System.out.println("The division of 24 and 5 = " + (Double.parseDouble(s1) / Double.parseDouble(s2)));
        System.out.println("The multiplication of 24 and 5 = " + (Integer.parseInt(s1) * Integer.parseInt(s2)));
        System.out.println("The remainder of 24 and 5 = " + (Integer.parseInt(s1) % Integer.parseInt(s2)));


        System.out.println("\n------TASK2------\n");

        int num = (int)(Math.random() * 35) + 1;

        if(num == 1 ||num == 2 ||num == 3 ||num == 5 ||num == 7 ||num == 11 ||num == 13 ||num == 17 ||num == 19 ||
                num == 23 ||num == 29 ||num == 31){
            System.out.println(num + " IS A PRIME NUMBER");

        }
        else{
            System.out.println(num + " IS NOT A PRIME NUMBER");
        }

        System.out.println("\n------TASK3------\n");

        int num1 = (int)(Math.random()* 50 + 1);
        int num2 = (int)(Math.random()* 50 + 1);
        int num3 = (int)(Math.random()* 50 + 1);

        System.out.println("Random number 1 = " + num1);
        System.out.println("Random number 2 = " + num2);
        System.out.println("Random number 3 = " + num3);

        int max = Math.max(num1,Math.max(num2 ,num3));
        int min = Math.min(num1,Math.min(num2 ,num3));

        System.out.println("Lowest number is = " + min);

        if(num1 < max && num1 > min)
            System.out.println("Middle number is =" + num1) ;
        else if(num2 < max && num2 > min)
            System.out.println("Middle number is =" + num2) ;
        else if(num3 < max && num3 > min)
            System.out.println("Middle number is =" + num3) ;

        System.out.println("Greatest number is =" + max);

        System.out.println("\n------TASK4------\n");

        char c1 = '5';
        int b1 = c1;


        if(b1 < 65 || b1 > 90 && b1 < 97)
            System.out.println("Invalid character detected");
        else if(b1 > 64 && b1 < 91)
            System.out.println("The letter is uppercase");
        else if(b1 > 96 && b1 < 123)
            System.out.println("The letter is lowercase");

        char c2 = 'a';
        int b2= c2;


        if(b2 < 65 || b2 > 90 && b2 < 97)
            System.out.println("Invalid character detected");
        else if(b2 > 64 && b2 < 91)
            System.out.println("The letter is uppercase");
        else if(b2 > 96 && b2 < 123)
            System.out.println("The letter is lowercase");

        char c3 = 'R';
        int b3 = c3;


        if(b3 < 65 || b3 > 90 && b3 < 97)
            System.out.println("Invalid character detected");
        else if(b3 > 64 && b3 < 91)
            System.out.println("The letter is uppercase");
        else if(b3 > 96 && b3 < 123)
            System.out.println("The letter is lowercase");


        System.out.println("\n------TASK5------\n");

        char c = '#';
        int character = c;

        if(character <= 57 || character >= 64 && character >= 96 ){
            System.out.println("Invalid character detected!!!");
        }
        else if (character == 65 || character ==69 || character == 73 || character == 79
        || character == 85 || character == 97 || character == 101 || character == 105 || character == 111 || character ==117){
            System.out.println("The letter is a vowel");
        }
        else{
            System.out.println("The letter is consonant");
        }


        System.out.println("\n------TASK6------\n");

        char char1 = '8';
        int chara = char1;

        if(chara >=32 || chara <= 57 || chara <= 90 || chara <=122 ){
            System.out.println("Invalid character detected!!!");
        }
        else{
            System.out.println("Special character is = " + chara);
        }


        System.out.println("\n------TASK7------\n");

        char l = 'g';
        int l1 = l;

        if(l1 > 64 && l1 < 91 || l1 > 96 && l1 < 122  ){
            System.out.println("Character is a letter.");
        }
        else if(l1 > 47 || l1 <= 57 && l1 <= 127){
            System.out.println("Character is a digit.");
        }
        else{
            System.out.println("Character is a special character.");
        }





















    }
}
