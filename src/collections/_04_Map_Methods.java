package collections;

import java.util.HashMap;

public class _04_Map_Methods {
    public static void main(String[] args) {
        /*
        Create a map to store countries with their capitals
        key           value
        Country       Capital City
         */

        HashMap<String, String> capitals = new HashMap<>();

        // How to add entries (Pairs)

        capitals.put("France","Paris");
        capitals.put("Italy","Rome");
        capitals.put("Spain","Madrid");

        // How to print a map
        System.out.println(capitals);

        // How to retrieve a value
        System.out.println(capitals.get("France"));
        System.out.println(capitals.get("Italy"));
        System.out.println(capitals.get("Spain"));


        // How to check if a map contains given key or value




    }
}
