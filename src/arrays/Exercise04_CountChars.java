package arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();

        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLetter(str.charAt(i))) {
                count++;
            }
        }
                      //OR

        char[] chars = str.toCharArray();
        for (char c : chars) {
            if(Character.isLetter(c)) count++;
        }
        System.out.println(count);

                     //OR

        count = 0;

        for (char c : ScannerHelper.getString().toCharArray()) {
            if(Character.isLetter(c)) count++;
        }

        System.out.println(count);


    }
}
