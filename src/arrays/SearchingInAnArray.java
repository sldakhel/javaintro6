package arrays;

import java.util.Arrays;

public class SearchingInAnArray {
    public static void main(String[] args) {
        int[] numbers = {3,10,8,5,5};


        //Check if this array has an element 7, print true if 7 exists, and false if it doesn't.
        // loop way

        boolean has7 = false;
        for (int number : numbers) {
            if(number == 7){
                has7 = true;
                break;
            }
            System.out.println(has7);

        }

        // binary search way
        // binary search cant be used without sorting


        Arrays.sort(numbers);
        System.out.println(Arrays.binarySearch(numbers,7));//returns the index of 7
    }
}
