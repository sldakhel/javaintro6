package arrays;

public class Exercise06_FirstEvenOdd {
    public static void main(String[] args) {

       int[] numbers = {0, 5, 3, 2, 4, 7, 10};
       // find the even and odd numbers

        for (int number : numbers) {
            if(number % 2 != 0){
                System.out.println(number);
                break;
            }

        }
        for (int number : numbers) {
            if(number % 2 == 0){
                System.out.println(number);
                break;
            }

        }

                       //OR Using one loop

        boolean isEvenFound = false;
        boolean isOddFound = false;

        for (int number : numbers) {
            if (number % 2 == 0) {
                System.out.println(number);
                isEvenFound = true;
            }
            else{
                System.out.println(number);
                isOddFound = true;
            }
            if(isEvenFound && isOddFound)break;

        }

    }
}
