package arrays;

import java.util.Arrays;

public class UnderstandingArrays {
    public static void main(String[] args) {
        String [] cities = {"Chicago", "Miami", "Toronto"};


        //The number of elements in the array

        System.out.println(cities.length);

        //Get particular elements from the array
        System.out.println(cities[1]);//Miami
        System.out.println(cities[0]);//Chicago
        System.out.println(cities[2]);//Toronto


        //Index out of bounds exception
        System.out.println(cities[-2]);//This index doesn't exist in our String array

        //How to print the array with all the elements
        //1.Convert your array to a string
        //2.Print it with print method
        System.out.println( Arrays.toString(cities));

        //How to loop an array
        for (String city : cities) {
            System.out.println(city);
        }

        //for each loop (enhanced loop)
        //always increases, it does not decrease
        //the : means "in"
        for(String element : cities){
            System.out.println(element);
        }




    }
}
