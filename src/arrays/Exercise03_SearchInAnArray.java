package arrays;

import java.util.Arrays;

public class Exercise03_SearchInAnArray {
    public static void main(String[] args) {
        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "iPad"};

         /*
           Check the collection you have above and print true if it contains Mouse
           Print false otherwise

          RESULT:
             true
         */

        boolean containsMouse = false;
        for (int i = 0; i < objects.length; i++) {
            // Check if the current element is equal to "Mouse"
            if (objects[i].equals("Mouse")) {
                containsMouse = true;
                break;
            }
        }
               //OR
        Arrays.sort(objects);

        System.out.println(Arrays.binarySearch(objects,"Mouse") >= 0); 

    }
}
