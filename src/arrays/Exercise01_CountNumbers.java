package arrays;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class Exercise01_CountNumbers {
    public static void main(String[] args) {
        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};
        //Write a program that counts how many negative numbers you in the array -> 2
         /*
        PSEUDO CODE
        Check each number one by one
        Count whenever a number is negative
        After checking all numbers, print the result
         */

        int negatives = 0;
        for (int number : numbers) {
            if (number < 0) negatives++;
            System.out.println(negatives);
        }

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] < 0) negatives++;
        }
        System.out.println(negatives);


        int evenNumb = 0;
        for (int number : numbers) {
            if (number % 2 == 0) evenNumb++;
        }
        System.out.println(evenNumb);

        //write a program that will find the sum of all the numbers in the array

        int sum = 0;

        for (int number : numbers) {
            sum += number;
        }
        System.out.println(sum);


    }
}