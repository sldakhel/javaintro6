package arrays;

import java.util.Arrays;

public class IntArray {
    public static void main(String[] args) {
        //Create an int array that will store 6 numbers

        int[] numbers = new int [6];
        System.out.println( Arrays.toString(numbers));

        //How to assign a value to an existing index
        //example: index of 0 -> 5
        //index of 2 -> 15
        //index of 4 -> 25

        numbers[0] = 5;
        numbers[2] = 15;
        numbers[4] = 25;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        for(int number : numbers){
            System.out.println(number);
        }
    }
}
