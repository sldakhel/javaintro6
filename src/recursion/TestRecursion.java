package recursion;

public class TestRecursion {
    public static void main(String[] args) {
        int[] numbers = {3, 5, 6};
        for (int n : numbers) {
            int result = factorialRecursive(n);

        }
    }
    public static int factorialRecursive(int n) {
        if (n == 0) {
            return 1;
        } else {
            return n * factorialRecursive(n - 1);
        }
    }


}



