package practices;

import utilities.ScannerHelper;

public class Exercise01_StringMethods {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();
        System.out.println("The string given is = " + str);

        if(str.isEmpty()) System.out.println("The string given is empty");
        else System.out.println("The length is = " + str.length());

                            //OR

        if(str.equals("")) System.out.println("The string given is empty");
                            //OR
        if(str.length()== 0) System.out.println("the string given is empty");
                            //OR
        if(str.length() < 1) System.out.println("the string given is empty");


        if(str.isEmpty()){
            System.out.println("There is no character in this string.");
        }
        else System.out.println("The first character = " + str.charAt(0));

        if(!str.isEmpty()){
            System.out.println("The last character is = " + str.charAt(str.length() -1));
        }
        else System.out.println("The string is empty");

        str = str.toLowerCase();
        if(str.contains("a") || str.contains("e") || str.contains("i") || str.contains("o") || str.contains("u")){
            System.out.println("The string has vowel");
        }
        else System.out.println("The string does not have vowel");






    }


    }

