package practices;

import utilities.ScannerHelper;

public class Exercise06_StringMethods {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();

        if(str.length() >= 4 && str.startsWith("xx") && str.endsWith("xx")) System.out.println(true);
        else if(str.length() < 4) System.out.println("INVALID INPUT");
        else System.out.println(false);
    }
}
