package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework13 {

    public static boolean hasLowerCase(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLowerCase(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static ArrayList<Integer> noZero(ArrayList<Integer> nums) {
        ArrayList<Integer> result = new ArrayList<>();

        for (Integer num : nums) {
            if (num != 0) {
                result.add(num);
            }
        }

        return result;
    }

    public static int[][] numberAndSquare(int[] nums) {
        int[][] result = new int[nums.length][2];

        for (int i = 0; i < nums.length; i++) {
            result[i][0] = nums[i];
            result[i][1] = nums[i] * nums[i];
        }

        return result;
    }

    public static boolean containsValue(String[] array, String value) {
        Arrays.sort(array);
        int index = Arrays.binarySearch(array, value);
        return (index >= 0);
    }

    public static String reverseSentence(String sentence) {
        String[] words = sentence.split(" ");

        if (words.length < 2) {
            return "There is not enough words!";
        }

        StringBuilder reversed = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            String word = words[i].toLowerCase();
            if (i == words.length - 1) {
                word = capitalizeFirstLetter(word);
            }
            reversed.append(word);
            if (i > 0) {
                reversed.append(" ");
            }
        }

        return reversed.toString();
    }

    public static String capitalizeFirstLetter(String word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }

    public static String removeStringSpecialsDigits(String input) {
        StringBuilder result = new StringBuilder();

        for (char c : input.toCharArray()) {
            if (Character.isLetter(c) || c == ' ') {
                result.append(c);
            }
        }

        return result.toString();
    }
}

