package homeworks;

public class Homework17 {
    public class WordExtractor {
        public String nthWord(String sentence, int n) {
            String[] words = sentence.split(" ");

            if (n > 0 && n <= words.length) {
                return words[n - 1];
            } else {
                return "";
            }

        }
    }

    public class ArmstrongNumber {
        public boolean isArmstrong(int number) {
            int originalNumber = number;
            int sum = 0;
            int digits = String.valueOf(number).length();

            while (number > 0) {
                int digit = number % 10;
                sum += Math.pow(digit, digits);
                number /= 10;
            }

            return sum == originalNumber;
        }
    }

    public class NumberReverser {
        public int reverseNumber(int number) {
            int reversedNumber = 0;

            while (number != 0) {
                int digit = number % 10;
                reversedNumber = reversedNumber * 10 + digit;
                number /= 10;
            }

            return reversedNumber;
        }
    }
}

