package homeworks;


import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("\n------TASK1------\n");

        System.out.println("Please enter a number.");
        int num1 = input.nextInt();

        System.out.println("Please enter your second number.");
        int num2 = input.nextInt();

        input.nextLine();
        int sum = num1 + num2;

        System.out.println("The number 1 entered by user is = " + num1);
        System.out.println("The number 2 entered by user is = " + num2);

        System.out.println("The sum of number 1 and number 2 entered is =" + sum);


        System.out.println("\n------TASK2------\n");

        System.out.println("Please enter your first number.");
        int d1 = input.nextInt();
        input.nextLine();

        System.out.println("please enter your second number.");
        int d2 = input.nextInt();
        input.nextLine();

        int product = d1 * d2;
        System.out.println("The product of the given 2 numbers is: " + product);


        System.out.println("\n------TASK3------\n");

        System.out.println("Please enter 2 floating numbers.");
        double input1 = input.nextDouble();
        double input2 = input.nextDouble();

        System.out.println("The sum of the given numbers is =" + (input1 + input2));
        System.out.println("The product of the given numbers is =" + (input1 * input2));
        System.out.println("The subtraction of the given numbers is =" + (input1 - input2));
        System.out.println("The division of the given numbers is =" + (input1 / input2));
        System.out.println("The remainder of the given numbers is =" + (input1 % input2));


        System.out.println("\n------TASK4------\n");

        System.out.println(-10 + 7 *5);
        System.out.println((72 + 24) % 24);
        System.out.println(10 + -3 *9 / 9);
        System.out.println(5 + 18 / 3 *3 - (6 % 3));


        System.out.println("\n------TASK5------\n");

        System.out.println("Pleas enter 2 numbers.");
        int input3 = input.nextInt();
        int input4 = input.nextInt();
        System.out.println("The average of the given numbers is:" + (input3 + input4) / 2);


       System.out.println("\n------TASK6------\n");

       System.out.println("Please enter 5 numbers.");
        int input5 = input.nextInt();
        int input6 = input.nextInt();
        int input7 = input.nextInt();
        int input8 = input.nextInt();
        int input9 = input.nextInt();

        System.out.println("The average of the given numbers is:" + ((input5 + input6 + input7 + input8 + input9) / 5));


        System.out.println("\n------TASK7------\n");

        System.out.println("Please enter 3 numbers.");
        int numb1 = input.nextInt();
        int numb2 = input.nextInt();
        int numb3 = input.nextInt();

        System.out.println("The" + numb1 + "multiplied with" + numb1 + "is =" + (numb1 * numb1));
        System.out.println("The" + numb2 + "multiplied with" + numb2 + "is =" + (numb2 * numb2));
        System.out.println("The" + numb3 + "multiplied with" + numb1 + "is =" + (numb3 * numb3));


        System.out.println("\n------TASK8------\n");

        System.out.println("Please enter a length of the side of a square.");
        int side = input.nextInt();

        System.out.println("Perimeter of the square = " + (4 * side));
        System.out.println("Area of the square = " + (side * side));


        System.out.println("\n------TASK9------\n");

        double sDETSalary = 90000;
        double threeYears = sDETSalary * 3;

        System.out.println("A Software Engineer in Test can earn $" + threeYears + "in 3 years.");

        System.out.println("\n------TASK10------\n");

        System.out.println("What is your favorite book?");
        String favBook = input.nextLine();
        System.out.println("What is your favorite color?");
        String favColor = input.nextLine();
        System.out.println("What is your favorite number?");
        int favNumber = input.nextInt();

        System.out.println("User's favorite book is:" + favBook);
        System.out.println("User's favorite color is:" + favColor);
        System.out.println("User's favorite number is:" + favNumber);


        System.out.println("\n------TASK11------\n");

        System.out.println("Please enter your first name:");
        String firstName = input.nextLine();

        System.out.println("Please enter your last name:");
        String lastName = input.nextLine();

        System.out.println("Please enter your age:");
        int age = input.nextInt();

        System.out.println("Please enter your email address:");
        String email = input.nextLine();

        System.out.println("Please enter your phone number:");
        String phone = input.nextLine();

        System.out.println("Please enter your address.");
        String address = input.nextLine();

        System.out.println("\tUser who joined this program is" + firstName +" " + lastName + "." + firstName +"'s age is" + age + ". " + firstName + "'s email \naddress is " + email + ", phone number is" + phone + "and address is\n" + address + "." );
















    }
}
