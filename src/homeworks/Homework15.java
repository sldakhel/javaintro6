package homeworks;

import java.util.HashSet;

public class Homework15 {

    //Task1
    public static int[] fibonacciSeries1(int n) {
        int[] fibonacci = new int[n];

        if (n >= 1) {
            fibonacci[0] = 0;
        }

        if (n >= 2) {
            fibonacci[1] = 1;
        }

        if (n >= 3) {
            for (int i = 2; i < n; i++) {
                fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
            }
        }

        return fibonacci;
    }
    //Task2
    public static int fibonacciSeries2(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("n must be a positive integer.");
        }

        if (n <= 2) {
            return n - 1;
        }

        int prev = 0;
        int curr = 1;

        for (int i = 3; i <= n; i++) {
            int next = prev + curr;
            prev = curr;
            curr = next;
        }

        return curr;
    }
    //Task3
    public static int[] findUniques(int[] arr1, int[] arr2) {
        HashSet<Integer> uniqueValues = new HashSet<>();

        for (int num : arr1) {
            uniqueValues.add(num);
        }

        for (int num : arr2) {
            uniqueValues.add(num);
        }

        int[] result = new int[uniqueValues.size()];
        int index = 0;
        for (int num : uniqueValues) {
            result[index++] = num;
        }

        return result;
    }
    //Task4
        public static int firstDuplicate(int[] arr) {
            HashSet<Integer> uniqueNumbers = new HashSet<>();

            for (int num : arr) {
                if (uniqueNumbers.contains(num)) {
                    return num;
                }
                uniqueNumbers.add(num);
            }

            return -1;
        }

    //Task5
    public static boolean isPowerOf3(int num) {
        if (num <= 0) {
            return false;
        }

        while (num % 3 == 0) {
            num /= 3;
        }

        return num == 1;
    }




}
