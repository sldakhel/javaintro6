package homeworks;

public class Homework20 {

    public static int sum(int[] arr, boolean isEven) {
        int count = 0;

        for (int num : arr) {
            if ((num % 2 == 0 && isEven) || (num % 2 != 0 && !isEven)) {
                count++;
            }
        }

        return count;
    }


    public static int sumDigitsDouble(String input) {
        int sum = 0;

        boolean digitsFound = false;
        input = input.replaceAll("\\s", "");

        for (char c : input.toCharArray()) {
            if (Character.isDigit(c)) {
                int digit = Character.getNumericValue(c);
                if (digit >= 0) {
                    digitsFound = true;
                    sum += digit;
                }
            }
        }

        if (!digitsFound) {
            return -1;
        }

        return sum * 2;
    }



    public static int countOccurrence(String str1, String str2) {
        str1 = str1.replaceAll("\\s", "").toLowerCase();
        str2 = str2.replaceAll("\\s", "").toLowerCase();

        int count = 0;

        if (str2.isEmpty()) {
            return 0;
        }

        while (str1.contains(str2)) {
            count++;
            str1 = str1.replaceFirst(str2, "");
        }

        return count;
    }
}
