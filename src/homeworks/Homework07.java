package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homework07 {
    public static void main(String[] args) {

        System.out.println("\n------Task1------\n");

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 23, 67, 23, 78));

        System.out.println(numbers.get(3));
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(2));
        System.out.println(numbers);


        System.out.println("\n------Task2------\n");

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Brown", "Red", "White", "Black", "Purple"));

        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);


        System.out.println("\n------Task3------\n");

        ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(23, -34, -56, 0, 89, 100));

        System.out.println(nums);
        Collections.sort(nums);
        System.out.println(nums);


        System.out.println("\n------Task4------\n");
        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));
        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);


        System.out.println("\n------Task5------\n");
        ArrayList<String> marvel = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panter", "Deadpool", "Captain America"));
        System.out.println(marvel);
        System.out.println(marvel.contains("Wolwerine"));


        System.out.println("\n------Task6------\n");
        ArrayList<String> avengers = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));
        Collections.sort(avengers);
        System.out.println(avengers);
        System.out.println(avengers.contains("Hulk") && avengers.contains("Iron Man"));


        System.out.println("\n------Task7------\n");
        ArrayList<String> characters = new ArrayList<>(Arrays.asList("A", "x", "$", "%", "9", "*", "+", "F", "G"));
        System.out.println(characters);
        System.out.println(characters.get(0));
        System.out.println(characters.get(1));
        System.out.println(characters.get(2));
        System.out.println(characters.get(3));
        System.out.println(characters.get(4));
        System.out.println(characters.get(5));
        System.out.println(characters.get(6));
        System.out.println(characters.get(7));
        System.out.println(characters.get(8));


        System.out.println("\n------Task8------\n");
        System.out.println(countM(new ArrayList<>(Arrays.asList("Desktop","Laptop","Mouse","Monitor","Mouse-Pad","Adapter"))));


    }

    public static int countM(ArrayList<String> electronics) {
        ArrayList<String> electronic = new ArrayList<>(Arrays.asList("Desktop","Laptop","Mouse","Monitor","Mouse-Pad","Adapter"));
        System.out.println(electronic);
        Collections.sort(electronic);
        int countM = 0;
        int countAE = 0;
        for (String s : electronic) {
            if(s.toLowerCase().contains("m"))countM++;
        }

        return countM;


    }

}
