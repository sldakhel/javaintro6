package homeworks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeSet;

public class Homework11 {
    //Task 1
    public static String noSpace(String input) {
        return input.replaceAll(" ", "");
    }

    public static String replaceFirstLast(String input) {

        if (input.length() < 2) {
            return "";
        }


        Map<Character, Integer> charFreq = new HashMap<>();


        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);


            if (i == 0 || i == input.length() - 1) {
                int freq = charFreq.getOrDefault(c, 0);
                charFreq.put(c, freq + 1);
            }
        }


        char[] newStr = new char[input.length()];
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);


            if (i == 0 || i == input.length() - 1) {
                int freq = charFreq.get(c);
                newStr[i] = Character.forDigit(freq, 10);
            } else {
                newStr[i] = c;
            }
        }
        return new String(newStr);


    }

    public static boolean hasVowel(String input) {
        input = input.toLowerCase();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
                return true;
            }
        }
        return false;
    }

    public static double averageOfEdges(int a, int b, int c) {
        TreeSet<Integer> numbers = new TreeSet<>();
        numbers.add(a);
        numbers.add(b);
        numbers.add(c);
        int min = numbers.first();
        int max = numbers.last();

        double average = (min + max) / 2.0;

        return average;
    }

    public static String[] noA(String[] input) {
        HashSet<String> elementsToReplace = new HashSet<>();
        elementsToReplace.add("A");
        elementsToReplace.add("a");

        for (int i = 0; i < input.length; i++) {
            String element = input[i];

            if (elementsToReplace.contains(element.substring(0, 1))) {
                // Replace the element with "###"
                input[i] = "###";
            }
        }
        return input;
    }


}
