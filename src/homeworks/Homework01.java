package homeworks;

public class Homework01 {
    public static void main(String[] args) {
        System.out.println("\n------TASK1------\n");
        /*
        JAVA = 74_01001010
               65_01000001
               86_01010110
               65_01000001
        SELENIUM= 83_01010011
                  69_01000101
                  76_01001100
                  69_01000101
                  78_01001110
                  73_01001001
                  85_01010101
                  77_01001101
         */
        System.out.println("\n------TASK2------\n");
        System.out.println("MhySl");

        System.out.println("\n------TASK3------\n");
        System.out.println("I start to practice \" JAVA\" today, and I like it.");
        System.out.println("The secret of getting ahead is getting started.");
        System.out.println("\"Don't limit yourself.\"");
        System.out.println("Invest in your dreams. Grind now. Shine later.");
        System.out.println("It's not the load that breaks you down, its the way you carry it.");
        System.out.println("The hard days are what make you stronger.");
        System.out.println("You can waste your lives drawing lines. Or you can live your life crossing them.");


        System.out.println("\n------TASK4------\n");
        System.out.println("\tJava is easy to write and easy to run-this is the foundational\n strength of Java and why many developers program in it. When you write\n Java once,you can run it almost anywhere at any time.\n\n\tJava can be used to create complete application that can run on a\n single computer or be distributed across servers and clients in a network.\n\n\tAs a result,you can use it to easily build mobile applications or\n run-on desktop applications that use different operating systems and\n servers, such as Linux or Windows.");


        System.out.println("\n------TASK5------\n");
        int myAge = 37;
        int myFavoriteNumber = 4;
        double myHeight = 5.8;
        double myWeight = 164.0;
        char myFavoriteLetter = 'A';

        System.out.println(myAge);
        System.out.println(myFavoriteNumber);
        System.out.println(myHeight);
        System.out.println(myWeight);
        System.out.println(myFavoriteLetter);



    }
}
