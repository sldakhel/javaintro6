package homeworks;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {

    // Task 1

    public static int countConsonants(String str) {
        String consonantsRegex = "[^aeiouAEIOU\\d\\W_]";
        Pattern pattern = Pattern.compile(consonantsRegex);
        Matcher matcher = pattern.matcher(str);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }


    // Task 2

    public static String[] wordArray(String str) {
        String wordRegex = "\\b\\w+\\b";
        Pattern pattern = Pattern.compile(wordRegex);
        Matcher matcher = pattern.matcher(str);
        List<String> wordsList = new ArrayList<>();
        while (matcher.find()) {
            wordsList.add(matcher.group());
        }
        return wordsList.toArray(new String[wordsList.size()]);
    }

    // Task 3

    public static String removeExtraSpaces(String str) {
        String trimmed = str.trim();
        String regex = "\\s+";
        String replacement = " ";
        return trimmed.replaceAll(regex, replacement);
    }

    // Task 4

    public static int count3OrLess() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a sentence: ");
        String input = scanner.nextLine();
        scanner.close();

        String wordRegex = "\\b\\w{1,3}\\b";
        Pattern pattern = Pattern.compile(wordRegex);
        Matcher matcher = pattern.matcher(input);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }

}



