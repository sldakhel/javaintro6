package homeworks;

import java.util.Arrays;

public class Homework09 {

    public static void main(String[] args) {

        System.out.println("==========Task01==========\n");
        int[] numbers1 = {-4, 0, -7, 0, 5, 10, 45, 45};
        System.out.println(firstDuplicatedNumber(numbers1));

        int[] numbers2 = {-8, 56, 7, 8, 65};
        System.out.println(firstDuplicatedNumber(numbers2));

        int[] numbers3 = {3, 4, 3, 3, 5, 5, 6, 6, 7};
        System.out.println(firstDuplicatedNumber(numbers3));


        System.out.println("\n==========Task02==========\n");
        String[] words1 = {"Z", "abc", "z", "123", "#"};
        System.out.println(firstDuplicatedString(words1));

        String[] words2 = {"xyz", "java", "abc"};
        System.out.println(firstDuplicatedString(words2));

        String[] words3 = {"a", "b", "B", "XYZ", "123"};
        System.out.println(firstDuplicatedString(words3));

        System.out.println("\n==========Task03==========\n");
        int[] numbers4 = {0, -4, -7, 0, 5, 10, 45, -7, 0};


        int[] numbers5 = {1, 2, 5, 0, 7};


        System.out.println("\n==========Task04==========\n");
        String[] words4 = {"A", "foo", "12", "Foo", "bar", "a", "a", "java"};


        String[] words5 = {"python", "foo", "bar", "java", "123"};


        System.out.println("\n==========Task05==========\n");
        String[] words6 = {"abc", "foo", "bar"};
        reversedArray(words6);

        String[] words7 = {"java", "python", "ruby"};
        reversedArray(words7);

        System.out.println("\n==========Task06==========\n");
        String str1 = "Java is fun";
        System.out.println(reverseStringWords(str1));

        String str2 = "Today is a fun day";
        System.out.println(reverseStringWords(str2));



    }

    public static int firstDuplicatedNumber(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i] == arr[j]) {
                    return arr[i];
                }
            }
        }
        return -1;
    }

    public static String firstDuplicatedString(String[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i].equalsIgnoreCase(arr[j])) {
                    return arr[i];
                }
            }
        }
        return "There is no duplicates";
    }





    public static void reversedArray(Object[] arr) {
        int n = arr.length;
        for (int i = 0; i < n / 2; i++) {
            Object temp = arr[i];
            arr[i] = arr[n - 1 - i];
            arr[n - 1 - i] = temp;
        }
        System.out.println(Arrays.toString(arr));
    }

    public static String reverseStringWords(String str) {
        String[] words = str.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            sb.append(new StringBuilder(word).reverse().toString()).append(" ");
        }
        return sb.toString().trim();
    }



}
