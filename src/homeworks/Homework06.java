package homeworks;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {

        System.out.println("\n------Task1------\n");

        int[] numbers = new int[10];

        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;

        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));


        System.out.println("\n------Task2------\n");

        String[] str = new String[5];

        str[1] = "abc";
        str[4] = "xyz";

        System.out.println(str[3]);
        System.out.println(str[0]);
        System.out.println(str[4]);
        System.out.println(Arrays.toString(str));


        System.out.println("\n------Task3------\n");

        int[] numbers1 = {23,-34,-56,0,89,100};
        System.out.println(Arrays.toString(numbers1));
        Arrays.sort(numbers1);
        System.out.println(Arrays.toString(numbers1));


        System.out.println("\n------Task4------\n");

        String[] countries = {"Germany","Argentina","Ukraine","Romania"};
        System.out.println(Arrays.toString(countries));
        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));


        System.out.println("\n------Task5------\n");

        String[] ToonDogs = {"Scooby Doo","Snoopy","Blue","Pluto","Dino","Sparky"};
        System.out.println(Arrays.toString(ToonDogs));
        Arrays.sort(ToonDogs);
        System.out.println(Arrays.binarySearch(ToonDogs,"Pluto")>= 0);


        System.out.println("\n------Task6------\n");

        String[] ToonCats = {"Garfield","Tom","Sylvester","Azrael"};
        Arrays.sort(ToonCats);
        System.out.println(ToonCats);
        System.out.println(Arrays.binarySearch(ToonCats, "Garfield")>= 0);
        System.out.println(Arrays.binarySearch(ToonCats, "Felix")>= 0);






    }
}
