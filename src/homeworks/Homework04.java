package homeworks;

import utilities.ScannerHelper;

import java.util.Scanner;


public class Homework04 {
    public static void main(String[] args) {

        System.out.println("\n------Task1------\n");
        String name = ScannerHelper.getFirstName();
        System.out.println("The length of the name is = " + name.length());
        System.out.println("The first character in the name is = " + name.charAt(0));
        System.out.println("The last character in the name is = " + name.substring(name.length()-1));
        System.out.println("The first 3 characters in the name are = " + name.substring(0,3));
        System.out.println("The last 3 characters in the name are = " + name.substring(name.length()-3));

        if(name.startsWith("A")||name.startsWith("a")){
            System.out.println("You are in the club!");

        }
        else System.out.println("Sorry, you are not in the club");


        System.out.println("\n------Task2------\n");

        Scanner scanner = new Scanner(System.in);

        System.out.print("Please enter your full address: ");
        String address = scanner.nextLine();

        if (address.toLowerCase().contains("chicago")) {
            System.out.println("You are in the club");
        } else if (address.toLowerCase().contains("des plaines")) {
            System.out.println("You are welcome to join the club");
        } else {
            System.out.println("Sorry, you will never be in the club");
        }


        System.out.println("\n------Task3------\n");

        System.out.println("Please enter your favorite country.");
        String favCountry = scanner.nextLine();
        favCountry = favCountry.toLowerCase();

        if(favCountry.contains("a")) System.out.println("A is there");
        else if(favCountry.contains("i")) System.out.println("I is there");
        else if(favCountry.contains("a") && favCountry.contains("i")) System.out.println("A and i are there");
        else System.out.println("A and i are not there");


        System.out.println("\n------Task4------\n");


        String str = " Java is FUN ";
        String str1 = "Java";
        String str2 = "is";
        String str3 = "FUN";
        System.out.println("The first word in the str is = " + str1.toLowerCase().trim());
        System.out.println("The second word in the str is = " + str2.toLowerCase().trim());
        System.out.println("The third word in the str is = " + str3.toLowerCase().trim());









    }
}
