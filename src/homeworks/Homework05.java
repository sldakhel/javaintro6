package homeworks;

import java.util.Scanner;

public class Homework05 {
    public static void main(String[] args) {

        System.out.println("\n------Task1------\n");
        for (int i = 1; i <= 100; i++) {
            if (i % 7 == 0) System.out.print(i + " - ");

        }

        System.out.println("\n------Task2------\n");
        for (int i = 1; i <= 50; i++) {
            if (i % 2 == 0 && i % 3 == 0)
                System.out.print(i + " - ");

        }
        System.out.println("\n------Task3------\n");
        for (int i = 100; i >= 50; i--) {
            if (i % 5 == 0) System.out.print(i + " - ");

        }

        System.out.println("\n------Task4------\n");
        for (int i = 0; i <= 7; i++) {
            int squareRoot = i * i;
            System.out.println("The square of " + i + " is " + squareRoot);

        }

        System.out.println("\n------Task5------\n");
        int sum = 0;
        for (int i = 1; i <= 10 ; i++) {
            sum += i;
        }
        System.out.print(sum);


        System.out.println("\n------Task6------\n");
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a positive integer: ");
        int num = input.nextInt();
        int factorial = 1;
        for (int i = 1; i <= num; i++) {
            factorial *= i;
        }
        System.out.println(num + "! = " + factorial);


        System.out.println("\n------Task7------\n");
        System.out.println("Please enter your full name.");
        String name = input.nextLine();
        int vowels = 0;
        for (int i = 0; i <= name.length()-1; i++) {
            if(name.toLowerCase().charAt(i)== 'a'|| name.toLowerCase().charAt(i)== 'e'|| name.toLowerCase().charAt(i)== 'i'|| name.toLowerCase().charAt(i)== 'o' || name.toLowerCase().charAt(i)== 'u') vowels++;

        }
        System.out.println("There are " + vowels +  " vowel letters in this full name");


        System.out.println("\n------Task8------\n");



















    }
}
