package homeworks;

public class Homework14 {
    public static void fizzBuzz1(int n) {
        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

    public static String fizzBuzz2(int n) {
        if (n % 3 == 0 && n % 5 == 0) {
            return "FizzBuzz";
        } else if (n % 3 == 0) {
            return "Fizz";
        } else if (n % 5 == 0) {
            return "Buzz";
        } else {
            return Integer.toString(n);
        }
    }

    public static int findSumNumbers(String input) {
        int sum = 0;
        String[] numbers = input.replaceAll("[^0-9\\-]+", " ").trim().split("\\s+");

        for (String number : numbers) {
            try {
                sum += Integer.parseInt(number);
            } catch (NumberFormatException ignored) {
                // Ignore invalid numbers and continue with the next number
            }
        }

        return sum;
    }

    public static int findBiggestNumber(String input) {
        int biggestNumber = 0;
        String[] numbers = input.replaceAll("[^0-9\\-]+", " ").trim().split("\\s+");

        for (String number : numbers) {
            try {
                int currentNumber = Integer.parseInt(number);
                if (currentNumber > biggestNumber) {
                    biggestNumber = currentNumber;
                }
            } catch (NumberFormatException ignored) {
                // Ignore invalid numbers and continue with the next number
            }
        }

        return biggestNumber;
    }

    public static String countSequenceOfCharacters(String input) {
        if (input.isEmpty()) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        int count = 1;

        for (int i = 1; i < input.length(); i++) {
            if (input.charAt(i) == input.charAt(i - 1)) {
                count++;
            } else {
                result.append(input.charAt(i - 1)).append(count);
                count = 1;
            }
        }

        result.append(input.charAt(input.length() - 1)).append(count);

        return result.toString();
    }
}