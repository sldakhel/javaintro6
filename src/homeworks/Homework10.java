package homeworks;

import java.util.ArrayList;

public class Homework10 {
    public static void main(String[] args) {

    }

    // Task 1
    public static int countWords(String str) {
        String[] words = str.split("\\s+");
        return words.length;
    }

    // Task 2
    public static int countA(String str) {
        int count = 0;

        for (int i = 0; i < str.length(); i++) {

            if (str.charAt(i) == 'A' || str.charAt(i) == 'a') {
                count++;
            }
        }

        return count;
    }

    //Task 3
    public static int countEven(ArrayList<Integer> list) {
        return (int) list.stream().filter(element -> element > 0).count();
    }

    //Task 4
    public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> list) {
        ArrayList<Integer> uniqueList = new ArrayList<>();
        for (Integer integer : list) {
            if (!uniqueList.contains(integer)) {
                uniqueList.add(integer);
            }
        }
        return uniqueList;
    }

    // Task 5
    public static ArrayList<String> removeDuplicateElements(ArrayList<String> list) {
        ArrayList<String> uniqueList = new ArrayList<>();
        for (String s : list) {
            if (!uniqueList.contains(s)) {
                uniqueList.add(s);
            }
        }
        return uniqueList;


    }

    // Task 6
    public static String removeExtraSpaces(String input) {
        input = input.trim();
        input = input.replaceAll("\\s+", " ");

        return input;
    }
}