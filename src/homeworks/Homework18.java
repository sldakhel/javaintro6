package homeworks;

public class Homework18 {

    public static int[] doubleOrTriple(int[] array, boolean doubleValue) {
        int[] result = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            if (doubleValue) {
                result[i] = array[i] * 2; // Double the element
            } else {
                result[i] = array[i] * 3; // Triple the element
            }
        }

        return result;
    }

    public static String splitString(String input, int splitSize) {
        if (input.length() % splitSize != 0) {
            return ""; // Return empty String if length cannot be divided evenly
        }

        StringBuilder result = new StringBuilder();
        int startIndex = 0;

        while (startIndex < input.length()) {
            String splitSubstring = input.substring(startIndex, startIndex + splitSize);
            result.append(splitSubstring).append(" ");
            startIndex += splitSize;
        }

        return result.toString().trim();
    }

    public static int countPalindrome(String input) {
        String[] words = input.split(" ");
        int count = 0;

        for (String word : words) {
            String reverseWord = new StringBuilder(word.toLowerCase()).reverse().toString();

            if (word.toLowerCase().equals(reverseWord)) {
                count++;
            }
        }

        return count;
    }
}
