package homeworks;

import java.util.Arrays;

public class Homework19 {

    public static int sum(int[] arr, boolean isEvenIndexes) {
        int sum = 0;
        int startIndex = isEvenIndexes ? 0 : 1;
        for (int i = startIndex; i < arr.length; i += 2) {
            sum += arr[i];
        }
        return sum;
    }

    public static String nthChars(String str, int n) {
        if (str.length() < n) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length(); i += n) {
            result.append(str.charAt(i));
        }

        return result.toString();
    }

    public static boolean canFormString(String str1, String str2) {
        str1 = str1.replaceAll("\\s", "").toLowerCase();
        str2 = str2.replaceAll("\\s", "").toLowerCase();

        char[] charArray1 = str1.toCharArray();
        char[] charArray2 = str2.toCharArray();
        Arrays.sort(charArray1);
        Arrays.sort(charArray2);

        return Arrays.equals(charArray1, charArray2);
    }

    public static boolean isAnagram(String str1, String str2) {
        str1 = str1.replaceAll("\\s", "").toLowerCase();
        str2 = str2.replaceAll("\\s", "").toLowerCase();

        char[] charArray1 = str1.toCharArray();
        char[] charArray2 = str2.toCharArray();
        Arrays.sort(charArray1);
        Arrays.sort(charArray2);

        return Arrays.equals(charArray1, charArray2);
    }
}
