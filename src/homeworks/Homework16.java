package homeworks;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Homework16 {
    public static void main(String[] args) {
        //Task 1

        String input = "{name}John{age}25{city}New York";

        if (input.startsWith("{") && input.endsWith("}")) {
            input = input.substring(1, input.length() - 1);
        }

        String[] entries = input.split("\\}\\{");

        Map<String, String> dataMap = new TreeMap<>();

        for (String entry : entries) {
            String[] keyValue = entry.split("\\{", 2);
            if (keyValue.length == 2) {
                String key = keyValue[0].trim();
                String value = keyValue[1].trim();
                dataMap.put(key, value);
            }
        }
        for (Map.Entry<String, String> entry : dataMap.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }

    }
   //Task 2
    public static double calculateTotalPrice1(Map<String, Integer> items) {
        Map<String, Double> itemPrices = new HashMap<>();
        itemPrices.put("Apple", 2.00);
        itemPrices.put("Orange", 3.29);
        itemPrices.put("Mango", 4.99);
        itemPrices.put("Pineapple", 5.25);

        double totalPrice = 0.0;

        for (Map.Entry<String, Integer> entry : items.entrySet()) {
            String item = entry.getKey();
            int amount = entry.getValue();

            if (itemPrices.containsKey(item)) {
                double price = itemPrices.get(item);
                totalPrice += price * amount;
            }
        }

        return totalPrice;
    }

    //Task 3
    public static double calculateTotalPrice2(Map<String, Integer> items) {
        Map<String, Double> itemPrices = new HashMap<>();
        itemPrices.put("Apple", 2.00);
        itemPrices.put("Orange", 3.29);
        itemPrices.put("Mango", 4.99);

        double totalPrice = 0.0;
        int appleCount = 0;
        int mangoCount = 0;

        for (Map.Entry<String, Integer> entry : items.entrySet()) {
            String item = entry.getKey();
            int amount = entry.getValue();

            if (itemPrices.containsKey(item)) {
                double price = itemPrices.get(item);

                if (item.equals("Apple")) {
                    appleCount += amount;
                    if (appleCount % 2 == 0) {
                        price *= 0.5;
                    }
                } else if (item.equals("Mango")) {
                    mangoCount += amount;
                }
                totalPrice += price * amount;
            }
        }


        int freeMangoCount = Math.min(mangoCount / 3, items.getOrDefault("Mango", 0) / 3);
        totalPrice -= itemPrices.get("Mango") * freeMangoCount;

        return totalPrice;
    }









            }
