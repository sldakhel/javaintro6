package string_Methods;

import java.util.Scanner;

public class Practice_01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 2 Strings.");
        String num1 = input.nextLine();
        String num2 = input.nextLine();

        if(num1.equals(num2)){
            System.out.println("These strings are equal");
        }
        else{
            System.out.println("These strings are not equal");
        }

    }
}
