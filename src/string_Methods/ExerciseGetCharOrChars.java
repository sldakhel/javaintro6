package string_Methods;

public class ExerciseGetCharOrChars {
    public static void main(String[] args) {

        String name1 = "bilal";//length 5 | middle character (char) is index of 2----- (to find the middle -1 divide by 2)
        String name2 = "Matthew";//length 7 | middle char is index of 3
        String name3 = "Ronaldo!!";//length 9 | middle char is index of 4
        // To find the middle character of an odd word we need to do (length() -1 /2

        System.out.println(name1.charAt(name1.length()-1/2));
        System.out.println(name2.charAt(name2.length()-1/2));
        System.out.println(name3.charAt(name3.length()-1/2));

        String name4 = "Okan";// length is 4 | middle character is index 1 and 2
        String name5 = "yousef"; //length is6 | middle character is index 2 and 3

        System.out.println(""+ name4.charAt(name4.length()/2 -1) + name4.charAt(name4.length()/2));
        System.out.println(""+ name5.charAt(name5.length()/2 -1) + name5.charAt(name4.length()/2));

        //To find the middle characters of an even word we would first get length()/2 -1
        //Then we would get the length()/2


    }


}
