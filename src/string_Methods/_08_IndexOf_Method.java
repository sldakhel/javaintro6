package string_Methods;

public class _08_IndexOf_Method {
    public static void main(String[] args) {

        /*
        1. return
        2. return an int
        3. non-static
        4. takes either a string or a char as an argument
         */
        String str = "TechGlobal";
        System.out.println(str.indexOf("h"));

        System.out.println(str.indexOf("y"));

        System.out.println(str.indexOf("Tech"));
        System.out.println(str.indexOf("Global"));

        System.out.println(str.indexOf("global"));

        System.out.println(str.indexOf("l"));

        //last Index of

    }

}
