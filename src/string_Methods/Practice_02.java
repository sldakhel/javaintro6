package string_Methods;

import java.util.Scanner;

public class Practice_02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your favorite book.");
        String favBook = input.nextLine();

        System.out.println("Please enter your favorite quote.");

        String favQuote = input.nextLine();

        int book = favBook.length();
        int quote = favQuote.length();
        System.out.println("The length of your favorite book is " + favBook.length());
        System.out.println("The length of your favorite quote is " + favQuote.length());


    }
}
