package string_Methods;

import utilities.ScannerHelper;

public class _13_Contains_Method {
    public static void main(String[] args) {

        /*
        1.return type
        2.returns boolean
        3.non-static
        4.it takes a string as an argument
         */
        String name ="John Doe";

        boolean containsJohn = name.contains("john");
        System.out.println(containsJohn);

        String str = ScannerHelper.getString().trim();

        System.out.println(str.contains(" ") && str.endsWith("."));


    }
}
