package string_Methods;

public class _03_Equals_Method {
    public static void main(String[] args) {
        /*
        1. Return type
        2. It's a return because it's returning a true or false
        3. It's nonstatic because we used the object, not the class.
        4. takes objects as an argument but in our case it takes String

         */
        String str1 = "Tech";
        String str2 = "Global";
        String str3 = "tech";

        boolean isEquals = str1.equals(str2);
        System.out.println(isEquals);// false

        System.out.println(str1.equals(str3));//false
    }
}
