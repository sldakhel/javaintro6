package string_Methods;

import utilities.ScannerHelper;

public class _12_startsAndEndsWith_Method {
    public static void main(String[] args) {
        /*
        1.return type
        2.returns a boolean
        3.non-static because its being called by the object
        4.it takes arguments (char or string)
         */
        String str = "TechGlobal";

        boolean startsWith = str.startsWith("T");
        boolean endsWith = str.endsWith("T");

        System.out.println(startsWith);
        System.out.println(endsWith);

        System.out.println(str.startsWith("Techgl"));
        System.out.println(str.endsWith("TechGlobal"));


    }
}
