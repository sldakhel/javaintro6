package string_Methods;

public class _11_Substring_Method {
    public static void main(String[] args) {
        /*
        1.return type
        2.returning a string
        3.non-static because we're calling it by the object
        4. it takes arguments (it takes int as an argument)
         */

        //TechGlobal
        //Tech is a substring of TechGlobal
        //Global is a substring of TechGlobal
        //ch is a substring of TechGlobal
        //echGlo is a substring of TechGlobal

        String str = "I love java";

        String firstWord = str.substring(0,1);
        String secondWord = str.substring(2,6);
        String thirdWord = str.substring(7,11);


        System.out.println(firstWord);
        System.out.println(secondWord);
        System.out.println(thirdWord);





    }
}
