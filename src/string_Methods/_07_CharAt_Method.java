package string_Methods;

public class _07_CharAt_Method {
    public static void main(String[] args) {
        /*
        1. return type
        2. returns char
        3.non-static because were calling it by the object name
        4. it takes an int index argument
         */
        String name = "Sandina";

        char firstLetter = name.charAt(0);
        char secondLetter = name.charAt(1);
        char thirdLetter = name.charAt(2);
        char fourthLetter = name.charAt(3);
        char fifthLetter = name.charAt(4);
        char sixthLetter = name.charAt(5);
        char seventhLetter = name.charAt(6);

        System.out.println(name);
        System.out.println(firstLetter);
        System.out.println(secondLetter);
        System.out.println(thirdLetter);
        System.out.println(fourthLetter);
        System.out.println(fifthLetter);
        System.out.println(sixthLetter);
        System.out.println(seventhLetter);





    }
}
