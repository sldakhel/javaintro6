package string_Methods;

public class Exercise_03 {
    public static void main(String[] args) {

        String strNew = "The best is Java";

        String firstWord = strNew.substring(12);
        System.out.println(firstWord);

                 //OR, if the sentence isn't hard coded

        firstWord = strNew.substring(strNew.indexOf("Java"));
        System.out.println(firstWord);
               //OR

        firstWord = strNew.substring(strNew.lastIndexOf(' ')).trim();
        System.out.println(firstWord);
    }
}
