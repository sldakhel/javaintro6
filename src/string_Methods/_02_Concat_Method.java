package string_Methods;

public class _02_Concat_Method {
    public static void main(String[] args) {
        /*
        1. It's a return type
        2. The return is a String
        3. It's nonstatic because it's invoked by the object name.

         */
        String str1 = "Tech";
        String str2 = "Global";

        System.out.println(str1.concat(str2));
    }
}
