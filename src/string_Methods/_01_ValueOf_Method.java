package string_Methods;

import utilities.ScannerHelper;

public class _01_ValueOf_Method {
    public static void main(String[] args) {
        /*
        1. return type
        2. returning a string
        3. it's static ( because you calling it with the class name. It's for the class. nonstatic is owned by the object.)
        4. it takes any type of variable as an argument.



        int num = 125;

        String.valueOf(num); // ---> "125"

        String numAsStr = String.valueOf(num);

        System.out.println(num + 5);//--->130
        System.out.println(numAsStr + 5);//--->1255

        char c = 'B';

        System.out.println(c);
        System.out.println(String.valueOf(c));
        */
        //int age = ScannerHelper.getAge();
       // System.out.println("Your age is = " + age);


        ScannerHelper.getColor();
    }

}
