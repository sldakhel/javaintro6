package string_Methods;

import java.sql.SQLOutput;

public class _06_ToUpperCase_Method {
    public static void main(String[] args) {
        /*
        1. return type
        2. returns string
        3. non-static method because we are not invoking the class name (string), were using the object (s1)
         */


        System.out.println("HelloWorld".toUpperCase());

        System.out.println(" ".toUpperCase());// empty string, will print out nothing

        String s1 = "HELLO";
        String s2 = "hello";

        if(s1.toUpperCase().equals(s2.toUpperCase())) System.out.println("EQUAL");
        else System.out.println("NOT EQUAL");
    }
}
