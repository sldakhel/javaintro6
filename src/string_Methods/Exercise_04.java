package string_Methods;

public class Exercise_04 {
    public static void main(String[] args) {
        // I go to TechGlobal


        String str = "I go to TechGlobal";

        String firstWord = str.substring(0,1);
        System.out.println(firstWord);

        String secondWord = str.substring(2,4);
        System.out.println(secondWord);

        String thirdWord = str.substring(5,7);
        System.out.println(thirdWord);

        String fourthWord = str.substring(8);
        System.out.println(fourthWord);
    }
}
