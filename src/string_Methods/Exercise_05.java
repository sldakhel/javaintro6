package string_Methods;

import utilities.ScannerHelper;


public class Exercise_05 {
    public static void main(String[] args) {
        String newWord = ScannerHelper.getString().toLowerCase();

        boolean startsWith = newWord.startsWith("a");
        boolean endsWith = newWord.endsWith("e");

        System.out.println(newWord.startsWith("a"));
        System.out.println(newWord.endsWith("e"));
               //OR
        System.out.println(newWord.startsWith("a") && newWord.endsWith("e"));
    }
}
