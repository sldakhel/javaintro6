package methods;

import utilities.MathHelper;
import utilities.Printer;

public class TestPrinterMethod {
    public static void main(String[] args) {
        Printer.printGM();
        Printer.printGM();

        Printer.helloName("Sandina");
    }


    // static vs non-static methods
    // static methods can be invoked with the class name----ex ClassName. only static methods will appear
    // non- static methods can be invoked with an object of the class

    Printer myPrinter = new Printer();





}
