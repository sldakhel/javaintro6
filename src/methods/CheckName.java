package methods;

import utilities.ScannerHelper;

public class CheckName {
    public static void main(String[] args) {
        //I can invoke static methods with class name
        //I can call non-static methods with objects


        ScannerHelper scannerHelper = new ScannerHelper();

        String name = scannerHelper.getFirstName();

        System.out.println("The name entered by user = " + name);

    }

}
