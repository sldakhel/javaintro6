package primitives;

public class FloatingNumbers {
    public static void main(String[] args) {

        float myFloat1 = 20.5F;
        double myDouble1 = 20.5;
        System.out.println(myFloat1);
        System.out.println(myDouble1);


        System.out.println("\n------floating numbers more------\n");
        float myFloat2 =10;
        double myDouble2 = 234235;


        System.out.println("\n-------floating numbers precision------\n");
        float f1 = 3274.234235423552F;
        double d1 = 3274.23423542352;



    }
}
