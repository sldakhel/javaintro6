package utilities;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Scanner;

public class ScannerHelper {
    static Scanner input = new Scanner(System.in);

    //Write a method that ask and return a name from user.



    public static String getFirstName(){
        System.out.println("Please enter a first name:");
        return input.nextLine();

    }


    public static int getAge(){
        System.out.println("Please enter an age:");
        return input.nextInt();
    }



    public static int getNumber(){
        System.out.println("Please enter a number");
        int number = input.nextInt();
        input.nextLine();
        return number;



    }

    public static String getString(){
        System.out.println("Please enter a String");
        String str = input.nextLine();

        return str;
    }

    public static String getColor(){
        System.out.println("Give me a color.");
        String color = input.nextLine();
        System.out.println("your color is " + color);
        return color ;


    }





}
