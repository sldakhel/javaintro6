package utilities;

public class Printer {

    // Write a method that prints Good morning.

    public static void printGM(){
        System.out.println("Good Morning");
    }


    // Write a public static method and print it with Hello, helloName

    public static void helloName(String name){
        System.out.println("Hello " + name);

    }

    //Write a public non-static method that prints TechGlobal

    public void printTechGlobal(){
        System.out.println("Tech Global");
    }


}
