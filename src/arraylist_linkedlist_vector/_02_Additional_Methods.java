package arraylist_linkedlist_vector;

import java.util.ArrayList;

public class _02_Additional_Methods {
    public static void main(String[] args) {
        /*
        Create an ArrayList to store below numbers
          10
          15
          20
          10
          20
          30
          Print the ArrayList
          Print the size

          EXPECTED:
         [10, 15, 20, 10, 20, 30]
           6
             */
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(10);
        numbers.add(20);
        numbers.add(30);
        System.out.println(numbers);
        System.out.println(numbers.size());
        /*
        check if it contains 5, 10, 20
         */

        System.out.println(numbers.contains(5));
        System.out.println(numbers.contains(10));
        System.out.println(numbers.contains(20));
        System.out.println(numbers.contains(45));

        /*
        what is the first occurrence of the index of the element of 10,and 20 and the last index
         */
        System.out.println(numbers.indexOf(10));
        System.out.println(numbers.indexOf(20));
        System.out.println(numbers.lastIndexOf(10));
        System.out.println(numbers.lastIndexOf(20));
        System.out.println(numbers.indexOf(15));
        System.out.println(numbers.lastIndexOf(15));
        System.out.println(numbers.indexOf(21));
        System.out.println(numbers.lastIndexOf(21));

        /*
         Remove 15 from the list
         */
        numbers.remove((Integer)15);
        numbers.remove((Integer)30);
        System.out.println(numbers);

        numbers.remove((Integer) 10);//will remove only the first 10

        numbers.removeIf(element -> element == 20);// Lambda expression
         /*
         remove all the elements
          */
        numbers.add(11);// adding more numbers to the list
        numbers.add(12);//adding more numbers to the list
        numbers.add(13);//adding more numbers to the list
        System.out.println(numbers);

        System.out.println(numbers.isEmpty());

        numbers.removeAll(numbers);// remove all is not great to use here. you use it mostly when you want to remove
                                   // more than one number ex, remove all the elements that equals 3,13,26
        numbers.clear();// will delete everything -> []



    }
}
