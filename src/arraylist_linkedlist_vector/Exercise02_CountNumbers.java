package arraylist_linkedlist_vector;

import java.util.ArrayList;

public class Exercise02_CountNumbers {
    public static void main(String[] args) {

    }
    public static int countEven(ArrayList<Integer> list) {
        int count = 0;
        for (Integer num : list) {
            if (num % 2 == 0) {
                count++;
            }
        }
        return count;
    }
    //public static int countEven(ArrayList<Integer> list){
       // return (int) list.stream().filter(element -> element % 2 == 0).count();

    public static long no3(ArrayList<Integer> list) {
        return list.stream().filter(num -> String.valueOf(num).contains("3")).count();
    }



}
