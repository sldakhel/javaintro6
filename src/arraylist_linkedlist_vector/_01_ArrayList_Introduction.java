package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _01_ArrayList_Introduction {
    public static void main(String[] args) {
        //1. How to create an Array vs. an ArrayList
        System.out.println("\n------Task1------\n");

        String[] array = new String[3];// syntax of an array

        ArrayList<String> list = new ArrayList<>();// syntax of an array list. The capacity will by default go to 10.

        //2. How to get the size of an array and array list.
        System.out.println("\n------Task2------\n");
        System.out.println("The size of the array = " + array.length);// which is 3
        System.out.println("The size of the array list is = " + list.size());//the size will be 0


        //3.How to print an array vs. array list
        System.out.println("\n------Task3------\n");
        System.out.println("The array = " + Arrays.toString(array));//[null,ull,null]
        System.out.println("The list = " + list);//it will be empty []

        //4. How to add elements to an array vs. array list
        System.out.println("\n------Task4------\n");
        array[1] = "Alex";
        array[2] = "Max";
        array[0] = "John";
        System.out.println("The array = " + Arrays.toString(array));

        list.add("Joe");
        list.add("Jane");// the element you add first will be the first one in the list,it won't make them alphabetically
        list.add("Mike");
        list.add("Adam");
        list.add(2,"Jazzy");//you can add an element to the specific index you want by using .add with index.
        System.out.println("The list = " + list);

        //5. How to update an existing element in an array vs. array list
        System.out.println("\n------Task5------\n");
        array[1] = "Ali";// this is how you can change what's in the index of 1 from alex to ali. it wil replace it
        System.out.println("The array = " + Arrays.toString(array));

        list.set(1,"Jasmine");// This will replace Jane with Jasmine.
        System.out.println("The list = " + list);

        //6. How to retrieve/ get an element from Array vs. ArrayList.
        System.out.println("\n------Task6------\n");
        System.out.println(array[2]);//Max
        System.out.println(list.get(3));//Mike

        //7.How to loop an array vs. an array list
        System.out.println("\n------Task7------\n");// for i loop & for each loop
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        for (String element : array) {
            System.out.println(element);
        }
        for (String element : list) {
            System.out.println(element);
        }
        /*
        for(dataType varName : element)
        use the variable name here if you want to print or do anything else
         */
        System.out.println("\n------Task8------ forEach() method\n");
        list.forEach(System.out::println);// Lambda expression, this loops and prints each element

    }
}
