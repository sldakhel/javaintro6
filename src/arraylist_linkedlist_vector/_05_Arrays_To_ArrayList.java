package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class _05_Arrays_To_ArrayList {
    public static void main(String[] args) {
        /*
        how to convert an array to an array list
        1. Arrays.asList()method
        2. Loops - manual way
        3. Collections.addAll(collection1, collection2); // it will add collection2 into collection1
         */

        System.out.println("\n------Way1 asLIst() method");
        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Berlin", "Paris", "Rome"));

        System.out.println("\n------Way2------\n");
        String[] countries = {"USA", "Germany", "Spain", "Italy"};
        ArrayList<String> list = new ArrayList<>();
        for (String country : countries) {
            list.add(country);
        }
        System.out.println(list);

        System.out.println("\n----------Way-3 Collections.addAll() method-------\n");
        list = new ArrayList<>();

        Collections.addAll(list, countries);

        System.out.println(list);
    }


    }

