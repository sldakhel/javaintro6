package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Practice06 {
    public static void main(String[] args) {

    }
    public static int[] double1(int[]arr){
        for (int i = 0; i < arr.length; i++) {
            arr[i] *= 2;
        }
        return arr;
    }
    public static int secondMax(ArrayList<Integer> list){//2, 3, 7, 1, 1, 7, 1
        Collections.sort(list);//1, 1, 1, 2, 3, 7, 7

        for (int i = list.size()-2; i >= 0 ; i--) {
            if(list.get(i) < list.get(list.size() - 1)) return list.get(i);
        }
        return 0;


    /*
    int max = Integer.MIN_VALUE;
    int secondMax = Integer.MIN_VALUE;

    //WAY1
    for (Integer n : list) {
        max = Math.max(max, n);
    }
    for (Integer n : list) {
        if(n > secondMax && n < max) secondMax = n;
    }

    //WAY2
    for (Integer n : list) {
        if(n > max) {
            secondMax = max;
            max = n;
        }
    }

     */


    }

}
