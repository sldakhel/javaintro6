package regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidUsername {
    public static void main(String[] args) {



                Scanner scanner = new Scanner(System.in);
                System.out.print("Enter a username: ");
                String username = scanner.nextLine();

                Pattern pattern = Pattern.compile("[a-zA-Z0-9]{5,10}");
                Matcher matcher = pattern.matcher(username);

                if (matcher.matches()) {
                    System.out.println("Valid username");
                } else {
                    System.out.println("Error! Username must be 5 to 10 characters long and can only contain letters and numbers.");
                }
            }
        }



