package regex;

import utilities.ScannerHelper;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CountWords {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter a sentence: ");
        String sentence = scanner.nextLine();

        Pattern pattern = Pattern.compile("[A-Za-z]{1,}");
        Matcher matcher = pattern.matcher(sentence);

        int count = 0;
        while (matcher.find()) {
            System.out.println(matcher.group());
            count++;
        }

        System.out.println("This sentence contains " + count + " words");
    }




    }

