package loops.practices;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        int num;
        do {
             num = ScannerHelper.getNumber();
        }while(num < 10);

    }
}
