package loops;

import utilities.ScannerHelper;

public class Exercise06_PrintEvenNumbersUsingScanner {
    public static void main(String[] args) {
        int first = ScannerHelper.getNumber();
        int second = ScannerHelper.getNumber();

        for (int i = Math.min(first,second); i <= Math.max(first,second); i++) {
            if(i % 2 == 0) System.out.println(i);

        }
    }
}
