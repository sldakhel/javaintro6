package loops;

public class UnderstandingLoops {
    public static void main(String[] args) {

        /*
        for loop syntax

        for (initialization; termination; update){
        //block of code}

        initialization -> start point
        termination -> stop point
        update -> increasing or decreasing -> increment and decrement operators
         */
        System.out.println("Hello World");
        for(int num = 0; num < 20; num++){
            System.out.println("Hello World!");
        }



    }
}
