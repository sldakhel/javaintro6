package loops;

import utilities.ScannerHelper;

public class Exercise08_CountCharacterInAString {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();
        /*
        start:0
        end:str.length()-1
        update:++
         */

         int count = 0;
        for (int i = 0; i <= str.length()-1 ; i++) {
           if(str.charAt(i) == 'A' || str.charAt(i) == 'a') count++;
        }
        System.out.println(count);
    }
}
