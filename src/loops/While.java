package loops;

import utilities.ScannerHelper;

public class While {
    public static void main(String[] args) {

        String name = ScannerHelper.getFirstName();
        int a = 1;
        while (name.length()>0){
            System.out.println("The " + a + " letter is " + name.charAt(0));
            a++;
            name = name.substring(1,name.length());
        };


    }
}
