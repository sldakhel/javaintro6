package loops;

public class Exercise09_FindSumOfNumbers {
    public static void main(String[] args) {
        /*
        start: 10
        end:   15
        update: ++
         */
        int start = 10;
        int end = 15;
        int sum = 0;

        for (int i = start; i <= end; i++) {
            sum += i;
        }

        System.out.println(sum);
    }

    }

