package loops;

import utilities.ScannerHelper;

public class Exercise12_ReverseString {
    public static void main(String[] args) {
        String name = ScannerHelper.getString();
        String reversedName = "";
        for (int i = name.length()-1; i >= 0 ; i--) {
            reversedName += name.charAt(i);
            System.out.println(reversedName);

            /*
            I don't understand why we cant just use for (int i = name.length()-1; i >= 0 ; i--)
            and then just print out System.out.print(name.charAt(i));
             */
        }



    }


}