package loops;

import utilities.ScannerHelper;

public class Exercise10_SumOfNumbersByUser {
    public static void main(String[] args) {
        int n1 = ScannerHelper.getNumber();
        int n2 = ScannerHelper.getNumber();
        int n3 = ScannerHelper.getNumber();
        int n4 = ScannerHelper.getNumber();
        int n5 = ScannerHelper.getNumber();
        System.out.println(n1 + n2 + n3 + n4 + n5);


        // for loop

        int sum = 0;
        for (int i = 1; i <=5 ; i++) {
            sum += ScannerHelper.getNumber();

        }
        System.out.println(sum);

        // while loop

        int start = 1;
        int sumWhile = 0;

        while(start <= 5){
            sumWhile = ScannerHelper.getNumber();
        }
        System.out.println(sumWhile);
    }

}
