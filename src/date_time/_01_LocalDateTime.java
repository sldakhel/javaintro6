package date_time;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class _01_LocalDateTime {
    public static void main(String[] args) {
        LocalDate currentDate = LocalDate.now();
        System.out.println(currentDate);

        LocalTime currentTime = LocalTime.now();
        System.out.println(currentTime);

        LocalDateTime currentDateTime = LocalDateTime.now();
        System.out.println(currentDateTime);

        LocalDate startDate = LocalDate.of(2023, 1, 23);
        LocalDate endDate = LocalDate.of(2023, 5, 2);

        Duration duration = Duration.between(startDate.atStartOfDay(), endDate.atStartOfDay());
        long days = duration.toDays();
        System.out.println(days);





    }
}
