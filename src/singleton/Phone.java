package singleton;

public class Phone {

    //instance variable which is also phone
    public static Phone phone;

    private Phone() {
        //default constructor
    }

    //Create a method that instantiates a Phone object and returns it
    public static Phone getPhone(){
        if(phone == null) phone = new Phone();
        return phone;
    }



}
